# output format:
## analyse.json (class Analyse):
```
{
	"general": IdMsgStats,
	# an IdMsgStats object about all messages

	"users": IdMsgStats[],
	# a list with IdMsgStats objects that are filtered by each user
	# it's sorted by the user with the most messages first

	"chats": IdMsgStats[]
	# a list with IdMsgStats objects that are filtered by each chat
	# it's sorted by the chat with the most messages first
}
```

## class IdMsgStats
```
{
	"id": int,
	# unique identifier in each list
	# in users it's the user id
	# in chats it's the chat id
	# in general it's just the string "general", because there is no list that requires uniqe ids

	"msg_stat": MessageCounter
	# the stats
}
```

## class MessageCounter
```
{
	"msg_cnt": int,
	"word_cnt": int,
	"total_msg_len": int,
	"weekly_msg_cnt": int[],
	"hourly_msg_cnt": int[]
}

```
