#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

cd ../external/bigdatabot/
RES=`LANG=en git pull | tr -d '[:space:]'`

new_file=`ls -t1 messages*.csv | head -n 1`
old_file=`cat ../../tmp/current_data_file_name.txt 2>/dev/null`
if [[ -n $old_file ]]; then
	# there is an old file
	if [[ $old_file = $new_file ]]; then
		# no updates
		update="FALSE";
	else
		# updates
		update="TRUE";
	fi
else
	update="TRUE";
fi

if [[ $update = "TRUE" ]]; then
	echo "# Updates"
	echo $new_file > ../../tmp/current_data_file_name.txt
	echo "copy new $new_file to data/data.csv"
	cp $new_file ../../data/data.csv
fi
