#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

cd ..

mkdir -p build
# (cd build; cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=YES ..)
(cd build; cmake ..)
if [[ ! -e compile_commands.json ]]; then
	ln -s build/compile_commands.json
fi
