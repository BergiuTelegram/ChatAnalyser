#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

cd ../build/;
cmake -DCMAKE_BUILD_TYPE=Debug .. && make
if [[ $? -eq 0 ]]; then
	cd ..
	time ./build/ChatAnalyser
else
	cd ..
fi
