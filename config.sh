echo "# update submodules:"
git submodule update --init --recursive

mkdir -p out

echo "# downloading datafiles:"
./tools/update_data.sh

echo "# copying libs:"
./tools/copy_libs.sh

echo "# generating cmake files"
./tools/cmake.sh
