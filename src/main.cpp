#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <algorithm>
#include <ctime>
#include <locale>
#include <iomanip>

#include "csv-parser.cpp"
#include "nlohmann-json.hpp"

using json = nlohmann::json;

const std::string weekday_name[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

struct MessageCounter
{
	int msg_cnt = 0;
	int word_cnt = 0;
	int total_msg_len = 0;
	int weekly_msg_cnt[7];
	int hourly_msg_cnt[24];

	MessageCounter() {
		int i;
		for (i = 0; i < 7; ++i) {
			weekly_msg_cnt[i] = 0;
		}
		for (i = 0; i < 24; ++i) {
			hourly_msg_cnt[i] = 0;
		}
	}

	~MessageCounter() {
	}

	void add_message(int word_cnt, int total_msg_len) {
		this->msg_cnt += 1;
		this->word_cnt += word_cnt;
		this->total_msg_len += total_msg_len;
	}

	void add_time(tm* tm) {
		weekly_msg_cnt[tm->tm_wday] += 1;
		hourly_msg_cnt[tm->tm_hour] += 1;
	}

	std::string wdays_to_str(bool one_line=true) {
		std::stringstream ss;
		int i;
		for (i = 0; i < 7; ++i) {
			if (!one_line) {
				ss << "  ";
			}
			ss << weekday_name[i] << ": " << weekly_msg_cnt[i];
			if (i!=6) {
				if (one_line) {
					ss << ", ";
				} else {
					ss << std::endl;
				}
			}
		}
		return ss.str();
	}

	std::string hours_to_str(bool one_line=false) {
		std::stringstream ss;
		int x;
		int i;
		for (i = 0; i < 24; ++i) {
			if (!one_line) {
				ss << "  ";
			}
			if (i<10){
				ss << "0" << i << ":00 - 0" << i;
			} else {
				ss << i << ":00 - " << i;
			}
			ss << ":59: " << hourly_msg_cnt[i];
			if (i!=23) {
				if (one_line) {
					ss << "; ";
				} else {
					ss << std::endl;
				}
			}
		}
		return ss.str();
	}

	friend std::ostream& operator<<(std::ostream& os, const MessageCounter& obj){
		os << "(msgs:" << obj.msg_cnt << ")"
			<< " (avg_msgs_len:" << obj.total_msg_len/obj.msg_cnt << ")"
			<< " (avg_words_per_msg:" << obj.word_cnt/obj.msg_cnt << ")"
			<< " (avg_words_len:" << obj.total_msg_len/obj.word_cnt << ")";
		return os;
	}

};

struct IdMsgStats
{
	std::string id;
	MessageCounter msg_stat;
	std::string class_name = "IdMsgStats";
	IdMsgStats(std::string id, std::string class_name): id(id), msg_stat(), class_name(class_name) {
	}
	friend std::ostream& operator<<(std::ostream& os, const IdMsgStats& obj){
		os << obj.class_name << " " << obj.id << " " << obj.msg_stat;
		return os;
	}

};

struct User: IdMsgStats
{
	User(std::string id): IdMsgStats(id, "User") {
	}
};

struct Chat: IdMsgStats
{
	Chat(std::string id): IdMsgStats(id, "Chat") {
	}
};

struct General: IdMsgStats
{
	General(std::string id): IdMsgStats(id, "General") {
	}
};


struct Analyser
{

	std::unique_ptr<General> general;
	std::unique_ptr<std::map<std::string, std::shared_ptr<User>>> users;
	std::unique_ptr<std::vector<std::shared_ptr<User>>> v_users;
	std::unique_ptr<std::map<std::string, std::shared_ptr<Chat>>> chats;
	std::unique_ptr<std::vector<std::shared_ptr<Chat>>> v_chats;

	Analyser() {
		this->general = std::make_unique<General>("General");
		this->users = std::make_unique<std::map<std::string, std::shared_ptr<User>>>();
		this->v_users = std::make_unique<std::vector<std::shared_ptr<User>>>();
		this->chats = std::make_unique<std::map<std::string, std::shared_ptr<Chat>>>();
		this->v_chats = std::make_unique<std::vector<std::shared_ptr<Chat>>>();
	}

	static int count_words(std::string text) {
		std::string delimiter = " \t\n";
		// there are no empty messages, so we can start with 1
		int cnt = 1;
		for (char i : text) {
			for (char c : delimiter) {
				if (i==c) {
					// split
					++cnt;
					break;
				}
			}
		}
		return cnt;
	}

	int add_message(
			const std::string csv_counter, const std::string message_id,
			const std::string chat_id, const std::string user_id,
			const std::string text, const std::string str_time) {
		// GET USER
		std::shared_ptr<User> user;
		if (users->count(user_id) == 0) {
			// new user
			auto user_pair = users->try_emplace(user_id, std::make_shared<User>(user_id));
			auto user_it = user_pair.first;
			user = user_it->second;
		} else {
			auto user_it = users->find(user_id);
			user = user_it->second;
		}
		// GET CHAT
		std::shared_ptr<Chat> chat;
		if (chats->count(chat_id) == 0) {
			// new chat
			auto chat_pair = chats->try_emplace(chat_id, std::make_shared<Chat>(chat_id));
			auto chat_it = chat_pair.first;
			chat = chat_it->second;
		} else {
			auto chat_it = chats->find(chat_id);
			chat = chat_it->second;
		}
		// calculate
		int word_cnt = count_words(text);
		int msg_len = text.length();
		tm *tm;
		{ // time
			long l_time = std::strtol(str_time.c_str(), NULL, 10);
			// can cause an overflow on some architectures
			time_t timenum = (time_t) l_time;
			tm = std::localtime(&timenum);
		}
		// assign
		general->msg_stat.add_message(word_cnt, msg_len);
		user->msg_stat.add_message(word_cnt, msg_len);
		chat->msg_stat.add_message(word_cnt, msg_len);
		general->msg_stat.add_time(tm);
		user->msg_stat.add_time(tm);
		chat->msg_stat.add_time(tm);
		return 0;
	}

	int read_csv(std::string filename) {
		std::ifstream in(filename);
		if (in.fail()) {
			cout << "File not found" << endl ;
			return 1;
		}
		std::vector<std::string> row;
		std::string counter;
		std::string message_id;
		std::string user_id;
		std::string group_id;
		std::string text;
		std::string time;
		while(in.good())
		{
			row = csv_read_row(in, ',');
			counter = row[0];
			message_id = row[1];
			group_id = row[2];
			user_id = row[3];
			text = row[4];
			time = row[5];
			add_message(counter, message_id, group_id, user_id, text, time);
		}
		in.close();
		return 0;
	}

	int maps_into_vector() {
		for (auto it = users->begin(); it != users->end(); ++it)
		{
			auto user = it->second;
			v_users->push_back(user);
		}
		for (auto it = chats->begin(); it != chats->end(); ++it)
		{
			auto chat = it->second;
			v_chats->push_back(chat);
		}
		auto cmp = [](std::shared_ptr<IdMsgStats> const & a, std::shared_ptr<IdMsgStats> const & b)
		{
			if (a->msg_stat.msg_cnt > b->msg_stat.msg_cnt) {
				return true;
			} else {
				return false;
			}
		};
		std::sort(v_users->begin(), v_users->end(), cmp);
		std::sort(v_chats->begin(), v_chats->end(), cmp);
		return 0;
	}

	int run() {
		int err;
		// read data
		err = read_csv("data/data.csv");
		if (err) {
			return 1;
		}
		// sort users into vector
		maps_into_vector();
		// show top 10
		print_stats();
		json_export_stats("out/analyse.json");
		return 0;
	}

	void json_export_stats(std::string filename) {
		json j = *this;
		{
			std::ofstream of(filename);
			of << j << endl;
		}
	}

	void print_stats() {
		std::stringstream ss;
		int i;
		ss << "Top 10 Users:" << std::endl;
		i = 0;
		for (auto it = v_users->begin(); it != v_users->end() && i<10; ++it, ++i) {
			auto user = it->get(); //user is shared_ptr
			ss << *user << std::endl;
			if (i==0) {
				ss << " Weekly Stats:" << std::endl;
				ss << user->msg_stat.wdays_to_str() << std::endl;
			}
			if (i==0) {
				ss << " Hourly Stats:" << std::endl;
				ss << user->msg_stat.hours_to_str() << std::endl;
			}
		}
		ss << std::endl << "Top 10 Chats:" << std::endl;
		i = 0;
		for (auto it = v_chats->begin(); it != v_chats->end() && i<10; ++it, ++i) {
			auto chat = it->get(); //user is shared_ptr
			ss << *chat << std::endl;
			if (i==0) {
				ss << " Weekly Stats:" << std::endl;
				ss << chat->msg_stat.wdays_to_str() << std::endl;
			}
			if (i==0) {
				ss << " Hourly Stats:" << std::endl;
				ss << chat->msg_stat.hours_to_str() << std::endl;
			}
		}
		ss << std::endl << "General Stats:" << std::endl;
		ss << "Total msgs: " << general->msg_stat.msg_cnt << std::endl;
		ss << "Total users: " << users->size() << std::endl;
		ss << "Total chats: " << chats->size() << std::endl;
		ss << "Avg msgs len: " << general->msg_stat.total_msg_len/general->msg_stat.msg_cnt << std::endl;
		ss << std::endl << "Amount msgs per weekday:" << std::endl;
		ss << "  " << general->msg_stat.wdays_to_str() << std::endl;
		ss << std::endl << "Amount msgs per hour:" << std::endl;
		ss << general->msg_stat.hours_to_str() << std::endl;
		std::cout << ss.str() << std::endl;
	}

	friend std::ostream& operator<<(std::ostream& os, const Analyser& b){
		os << "Total msgs: " << b.general->msg_stat.msg_cnt;
		return os;
	}

};

void to_json(json& j, const MessageCounter& p)
{
	j = json{
		{"msg_cnt", p.msg_cnt},
		{"word_cnt", p.word_cnt},
		{"total_msg_len", p.total_msg_len},
		{"weekly_msg_cnt", p.weekly_msg_cnt},
		{"hourly_msg_cnt", p.hourly_msg_cnt}
	};
}

void to_json(json& j, const IdMsgStats& p)
{
	j = json{
		{"id", p.id},
		{"msg_stat", p.msg_stat}
	};
}

void to_json(json& j, const Analyser& p)
{
	int i;
	j = json{
		{"general", *p.general},
	};
	for(auto user: *p.v_users) {
		j["users"].push_back(*user);
	}
	for(auto chat: *p.v_chats) {
		j["chats"].push_back(*chat);
	}
}

int main(int argc, char *argv[])
{
	Analyser analyser;
	analyser.run();
	return 0;
}
