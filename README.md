## References
- csv-parser.cpp:
	- [zedwood.com/article/cpp-csv-parser](http://www.zedwood.com/article/cpp-csv-parser) ([CC-By-Sa 3.0](http://www.zedwood.com/article/cpp-csv-parser))
- json.hpp:
	- [github.com/nlohman/json](https://github.com/nlohmann/json/releases/download/v3.1.2/json.hpp) ([MIT License](http://opensource.org/licenses/MIT))
